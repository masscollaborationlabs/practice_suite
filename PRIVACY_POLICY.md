## Practice Suite: Privacy policy

Welcome to the Practice Suite app for Android and iOS!

This is an open source app developed by Berker Sen. The source code is available on Codeberg under the MIT license; the app is also available on Google Play Store and Apple App Store.

As an Android user myself, I take privacy very seriously.
I know how irritating it is when apps collect your data without your knowledge.

I hereby state, to the best of my knowledge and belief, that I have not programmed this app to collect any personally identifiable information. All data (app preferences (like theme, etc.) and log entries & recordings) created by the you (the user) is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it.

### Explanation of permissions requested in the app

The list of permissions required by the app can be found in the `AndroidManifest.xml` and `Info.plist` files:

https://codeberg.org/Berker/practice_suite/src/branch/master/android/app/src/main/AndroidManifest.xml
<br/>
https://codeberg.org/Berker/practice_suite/src/branch/master/ios/Runner/Info.plist

<br/>

|                  Permission                  | Why it is required                                                                                                                                                                                                                                                                                                                                                                                                   |
|:--------------------------------------------:|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|  `android.permission.READ_EXTERNAL_STORAGE`  | This is one of the sensitive permission that the app requests, and can be revoked by the system or the user at any time. This is required only if you want to use the recording function of the app. In order to replay the audio files you recorded, the app needs permission to read the storage. Recording some audio and revoking this permission later and trying to replay audio _may_ cause the app to crash. |
| `android.permission.WRITE_EXTERNAL_STORAGE`  | Another sensitive permission that the app requests, and can be revoked by the system or the user at any time. This is also required only if you want to use the recording function of the app. In order to save the audio files you recorded, the app needs permission to write to the storage.                                                                                                                      |
|      `android.permission.RECORD_AUDIO`       | This permission is needed for the tuner as well as the recording function to use the devices microphone as input source and can be revoked by the system or the user at any time.                                                                                                                                                                                                                                    |


 <hr style="border:1px solid gray">

If you find any security vulnerability that has been inadvertently caused by me, or have any question regarding how the app protects your privacy, please send me an email or post a discussion on Codeberg, and I will surely try to fix it/help you.

Yours sincerely,  
Berker Sen
<br/>
practice_suite@fastmail.com
