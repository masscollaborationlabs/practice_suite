part of 'tuner_bloc.dart';

abstract class TunerEvent extends Equatable {
  const TunerEvent();

  @override
  List<Object?> get props => [];
}

class InitTunerEvent extends TunerEvent {}
class DisposeTunerEvent extends TunerEvent {}

class ToggleTunerEvent extends TunerEvent {}
class LoadingStateTunerEvent extends TunerEvent {}

class UpdateNoteTunerEvent extends TunerEvent {
  final String note;
  final TuningStatus pitchStatus;

  const UpdateNoteTunerEvent({required this.note, required this.pitchStatus});

  @override
  String toString() {
    return 'UpdateNoteTunerEvent{note: $note, pitchStatus: $pitchStatus}';
  }
}

class ChangePitchTunerEvent extends TunerEvent {
  final double pitch;

  const ChangePitchTunerEvent({required this.pitch});
}
