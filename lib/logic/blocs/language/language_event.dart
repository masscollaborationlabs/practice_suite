part of 'language_bloc.dart';

abstract class LanguageEvent extends Equatable {
  const LanguageEvent();
}

class ChangeLanguageEvent extends LanguageEvent {
  final Locale appLanguage;


  const ChangeLanguageEvent({required this.appLanguage});

  @override
  List<Object?> get props => [];

}
