import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:practice_suite/presentation/screens/settings_screen.dart';

import '../../screens/statistics_screen.dart';

class AppBarPT extends StatefulWidget implements PreferredSizeWidget {
  const AppBarPT({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(50);

  @override
  State<AppBarPT> createState() => _AppBarPTState();
}

class _AppBarPTState extends State<AppBarPT> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: Image.asset(Theme.of(context).brightness == Brightness.light ?
        "assets/icons/p_t_for_light.png" : "assets/icons/p_t_for_dark.png",
      ),
      actions: [
        IconButton(
          onPressed: () => Navigator.of(context).push(
              CupertinoPageRoute(builder: (_) => const StatisticsScreen())),
          icon: const Icon(Icons.bar_chart),
        ),
        IconButton(
            onPressed: () => Navigator.of(context).push(
                CupertinoPageRoute(builder: (_) => const SettingsScreen())),
            icon: const Icon(Icons.settings)),
      ],
    );
  }
}
