enum TuningStatus {
  tunedMinus,
  tuned,
  tunedPlus,
  lowMinus,
  low,
  lowPlus,
  highMinus,
  high,
  highPlus,
  tooLowMinus,
  tooLow,
  tooLowPlus,
  tooHighMinus,
  tooHigh,
  tooHighPlus,
  undefined
}
