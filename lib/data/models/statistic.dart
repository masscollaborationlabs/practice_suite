class Statistic {
  AppSection section;
  String title;
  String value;

  Statistic({
    required this.section,
    required this.title,
    required this.value,
  });
}

enum AppSection {
  practiceLog,
  metronome,
  tuner,
  recording,
}
