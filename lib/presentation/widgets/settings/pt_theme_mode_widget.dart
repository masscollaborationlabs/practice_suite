import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../generated/l10n.dart';
import '../../../logic/blocs/app_theme/app_theme_bloc.dart';
import '../../../services/locator_service.dart';
import '../../styles/app_theme.dart';

class PTThemeModeWidget extends StatefulWidget {
  const PTThemeModeWidget({Key? key}) : super(key: key);

  @override
  State<PTThemeModeWidget> createState() => _PTThemeModeWidgetState();
}

class _PTThemeModeWidgetState extends State<PTThemeModeWidget> {
  ThemeMode _appTheme = AppTheme.initialTheme;
  bool _isSystemMode = true;
  bool _isDarkMode = false;
  bool _isLightMode = false;

  void selectTheme(ThemeMode selectedTheme) {
   _appTheme = selectedTheme;

    getIt<AppThemeBloc>().add(ChangeThemeEvent(themeMode: _appTheme));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppThemeBloc, AppThemeState>(
      builder: (context, state) {
        if (state is ChangeThemeState) {
          switch(state.themeMode) {
            case ThemeMode.system:
              _isSystemMode = true;
              _isDarkMode = false;
              _isLightMode = false;
              break;
            case ThemeMode.dark:
              _isSystemMode = false;
              _isDarkMode = true;
              _isLightMode = false;
              break;
            case ThemeMode.light:
              _isSystemMode = false;
              _isDarkMode = false;
              _isLightMode = true;
              break;
          }
        }
        return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Text(S.of(context).settings_theme_mode),
              ),
              Row(
                children: [
                  ChoiceChip(
                    selected: _isLightMode,
                    label: const Icon(Icons.light_mode_outlined),
                    onSelected: (value) {
                      selectTheme(ThemeMode.light);
                    },
                  ),
                  const SizedBox(width: 10),
                  ChoiceChip(
                    selected: _isDarkMode,
                    label: const Icon(Icons.dark_mode_outlined),
                    onSelected: (value) {
                      selectTheme(ThemeMode.dark);
                    },
                  ),
                  const SizedBox(width: 10),
                  ChoiceChip(
                    selected: _isSystemMode,
                    label: const Icon(Icons.system_security_update_good_outlined),
                    onSelected: (value) {
                      selectTheme(ThemeMode.system);
                    },
                  ),
                  const SizedBox(width: 15),
                ],
              )
            ],
        );
      },
    );
  }
}
