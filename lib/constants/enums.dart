import 'dart:ui';

enum EntryType {
  practiceLog,
  recordingLog,
}

enum PTLanguage {
  english('English', 'en_US', Locale('en', 'US')),
  deutsch('German','de_DE', Locale('de', 'DE')),
  turkce('Turkish', 'tr_TR', Locale('tr', 'TR'));

  const PTLanguage(this.name, this.localeName, this.locale);
  final String name;
  final String localeName;
  final Locale locale;
}