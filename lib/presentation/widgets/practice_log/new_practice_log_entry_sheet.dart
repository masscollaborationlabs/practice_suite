import 'package:date_time_picker/date_time_picker.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:practice_suite/data/models/practice_log_entry.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_container.dart';
import 'package:uuid/uuid.dart';
import 'package:practice_suite/generated/l10n.dart';

import '../../../services/locator_service.dart';

class NewPracticeLogEntrySheet extends StatefulWidget {
  final PracticeLogEntry? newLogEntry;

  const NewPracticeLogEntrySheet({Key? key, this.newLogEntry})
      : super(key: key);

  @override
  State<NewPracticeLogEntrySheet> createState() =>
      _NewPracticeLogEntrySheetState();
}

class _NewPracticeLogEntrySheetState extends State<NewPracticeLogEntrySheet> {
  final _durationFocusNode = FocusNode();
  final _descFocusNode = FocusNode();
  final _dateFocusNode = FocusNode();
  final _metronomeFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  final _tagFocusNode = FocusNode();
  bool _isEditing = false;
  bool _isAddingTag = false;
  TextEditingController tagTextController = TextEditingController();
  final bool _isIOS = Platform.isIOS;
  late Map<String, bool> availableTags;
  bool _isTagValidated = false;

  PracticeLogEntry _newEntry = PracticeLogEntry(
      id: const Uuid().v4(), title: "", duration: "", date: "", tags: []);

  bool _saveNewLogEntry() {
    final isValid = _form.currentState!.validate();
    if (isValid) {
      _newEntry.tags = [];
      availableTags.forEach((tag, isSelected) {
        if (isSelected) {
          _newEntry.tags.add(tag);
        }
      });
      _form.currentState!.save();
      return true;
    }
    return false;
  }

  String? validateTag(String tagText) {
    _isTagValidated = false;
    if (tagText.trim() == "") {
      _isTagValidated = false;
      tagTextController.clear();
      return S.of(context).practice_log_error_empty_tag;
    }
    _isTagValidated = true;
    return null;
  }

  void saveTag() {
    if (_isTagValidated) {
      if (availableTags.keys.contains(tagTextController.text)) {
        availableTags[tagTextController.text] = true;
      } else {
        availableTags.putIfAbsent(tagTextController.text, () => true);
      }
      tagTextController.clear();
    }
  }

  @override
  void initState() {
    availableTags = getIt<PracticeLogBloc>().availableTags;

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.newLogEntry != null) {
      _newEntry = widget.newLogEntry!;
      for (var tag in _newEntry.tags) {
        if (availableTags.containsKey(tag)) {
          availableTags[tag] = true;
        }
      }
      _isEditing = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(25))),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 12, bottom: 10),
                child: Text(
                    _isEditing
                        ? S.of(context).editLogEntry
                        : S.of(context).newLogDialogTitle,
                    style: Theme.of(context).textTheme.displayLarge),
              ),
              Form(
                key: _form,
                child: Column(
                  children: [
                    TextFormField(
                      initialValue: _newEntry.title,
                      decoration: InputDecoration(
                        labelText: S.of(context).title,
                        border: const OutlineInputBorder(),
                      ),
                      textInputAction: TextInputAction.next,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(_durationFocusNode);
                      },
                      validator: (value) {
                        if (value!.isEmpty) {
                          return S.of(context).titleValidation;
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _newEntry.title = value.toString();
                      },
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Expanded(
                          child: !_isAddingTag
                              ? PTContainer(
                                  padding: EdgeInsets.zero,
                                  borderColor: Colors.grey,
                                  child: SizedBox(
                                    width: double.infinity,
                                    height: 57,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0),
                                      child: availableTags.isEmpty
                                          ? Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                S.of(context).tagsEmpty,
                                                style: const TextStyle(
                                                    color: Colors.grey),
                                              ))
                                          : ListView(
                                              scrollDirection: Axis.horizontal,
                                              children:
                                                  availableTags.keys.map((tag) {
                                                return Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 8.0),
                                                  child: FilterChip(
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                          side: BorderSide(
                                                              color: Theme.of(
                                                                      context)
                                                                  .hintColor)),
                                                      label: Text(tag),
                                                      checkmarkColor: Theme
                                                              .of(context)
                                                          .listTileTheme
                                                          .textColor,
                                                      selectedColor: Theme
                                                              .of(context)
                                                          .cardColor,
                                                      backgroundColor: Theme
                                                              .of(context)
                                                          .cardColor,
                                                      selected:
                                                          availableTags[tag]!,
                                                      onSelected: (bool value) {
                                                        setState(() {
                                                          availableTags[tag] =
                                                              value;
                                                        });
                                                      }),
                                                );
                                              }).toList()),
                                    ),
                                  ))
                              : TextField(
                                  onChanged: (value) {
                                    setState(() {});
                                  },
                                  focusNode: _tagFocusNode,
                                  controller: tagTextController,
                                  decoration: InputDecoration(
                                    labelText: S.of(context).newTag,
                                    border: const OutlineInputBorder(),
                                    errorText: _tagFocusNode.hasFocus
                                        ? validateTag(tagTextController.text)
                                        : null,
                                  ),
                                ),
                        ),
                        const SizedBox(width: 8),
                        SizedBox(
                          height: 59,
                          width: 50,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(5.0),
                            onTap: () {
                              if (!_isAddingTag) {
                                _tagFocusNode.requestFocus();
                              }
                              validateTag(tagTextController.text);
                              if (_isTagValidated) {
                                saveTag();
                                setState(() {
                                  _isAddingTag = !_isAddingTag;
                                });
                              } else {
                                setState(() {
                                  _isAddingTag = !_isAddingTag;
                                });
                              }
                            },
                            child: PTContainer(
                                padding: EdgeInsets.zero,
                                borderColor: Colors.grey,
                                child: Icon(
                                  _isAddingTag ? Icons.check : Icons.add,
                                  color: tagTextController.text.isEmpty
                                      ? Colors.grey
                                      : Theme.of(context).colorScheme.primary,
                                )),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextFormField(
                              initialValue: _newEntry.duration,
                              decoration: InputDecoration(
                                errorMaxLines: 2,
                                labelText: S.of(context).duration,
                                border: const OutlineInputBorder(),
                              ),
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(_metronomeFocusNode);
                              },
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return S.of(context).durationValidation;
                                }
                                if (int.tryParse(value) == null) {
                                  return S
                                      .of(context)
                                      .duration_format_validation;
                                }
                                if (int.tryParse(value) != null &&
                                        int.tryParse(value)! > 720 ||
                                    int.tryParse(value)! < 1) {
                                  return S
                                      .of(context)
                                      .duration_amount_validation;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _newEntry.duration = value.toString();
                              },
                            ),
                          ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: TextFormField(
                              initialValue: _newEntry.metronome,
                              decoration: InputDecoration(
                                errorMaxLines: 2,
                                labelText: S.of(context).navBarMetronome,
                                border: const OutlineInputBorder(),
                              ),
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (_) {
                                FocusScope.of(context)
                                    .requestFocus(_dateFocusNode);
                              },
                              validator: (value) {
                                if (int.tryParse(value!) == null) {
                                  return S
                                      .of(context)
                                      .metronome_format_validation;
                                }
                                if (int.tryParse(value) != null &&
                                        int.tryParse(value)! > 230 ||
                                    int.tryParse(value)! < 24) {
                                  return S
                                      .of(context)
                                      .metronome_limit_validation;
                                }
                                return null;
                              },
                              onSaved: (value) {
                                _newEntry.metronome = value.toString();
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: DateTimePicker(
                        //Don't need a validator because DateTime.now is chosen as initial
                        focusNode: _dateFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_descFocusNode);
                        },
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: S.of(context).date),
                        type: DateTimePickerType.date,
                        dateMask: 'd MMM, yyyy',
                        initialValue: _isEditing
                            ? _newEntry.date
                            : DateTime.now().toIso8601String(),
                        firstDate: DateTime(DateTime.now().year - 1),
                        lastDate: DateTime.now(),
                        icon: const Icon(Icons.event),
                        dateLabelText: S.of(context).date,
                        onSaved: (val) {
                          _newEntry.date = DateFormat("yyyyMMdd")
                              .format(DateTime.parse(val!));
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: TextFormField(
                        initialValue: _newEntry.description,
                        maxLines: 3,
                        decoration: InputDecoration(
                          labelText: S.of(context).description,
                          border: const OutlineInputBorder(),
                        ),
                        textInputAction: TextInputAction.done,
                        onSaved: (value) {
                          if (value!.isEmpty) {
                            _newEntry.description =
                                S.of(context).optionalDescriptionText;
                          } else {
                            _newEntry.description = value.toString();
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(S.of(context).cancel),
                    ),
                    TextButton(
                      onPressed: () {
                        if (_saveNewLogEntry()) {
                          getIt<PracticeLogBloc>().add(_isEditing
                              ? UpdatePracticeLogEntry(_newEntry)
                              : AddPracticeLogEntry(_newEntry));
                          Navigator.of(context).pop();
                        }
                      },
                      child: Text(S.of(context).done),
                    )
                  ],
                ),
              ),
              if (_isIOS) const SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
