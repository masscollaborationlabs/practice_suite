import 'package:practice_suite/data/api/database_api.dart';
import 'package:practice_suite/data/models/practice_log_entry.dart';

class PracticeLogRepository {
  Future<List<PracticeLogEntry>> fetchAndSetLogEntries() async {
    final entryData = await DatabaseApi.getData("practice_log_entries");
    List<PracticeLogEntry> practiceLog = entryData
        .map(
          (entry) => PracticeLogEntry(
            id: entry["id"],
            title: entry["title"],
            duration: entry["duration"],
            metronome: entry["metronome"],
            date: entry["date"],
            description: entry["description"],
            tags: entry["tags"].toString().split("|"),
          ),
        )
        .toList();
    return practiceLog;
  }

  List<String> getAllTags(List<PracticeLogEntry> log) {
    List<String> tags = [];
    for (var logEntry in log) {
      for (var tag in logEntry.tags) {
        if (!tags.contains(tag)) {
          tags.add(tag);
        }
      }
    }
    return tags;
  }

  PracticeLogEntry getLogEntryById(
      String id, List<PracticeLogEntry> practiceLog) {
    return practiceLog.firstWhere((element) => element.id == id);
  }

  void addLogEntry(PracticeLogEntry newEntry) {
    DatabaseApi.insert("practice_log_entries", {
      "id": newEntry.id,
      "title": newEntry.title,
      "duration": newEntry.duration,
      "metronome": newEntry.metronome,
      "date": newEntry.date,
      "description": newEntry.description,
      "tags": newEntry.tags.join("|"),
    });
  }

  void removeLogEntry(String entryId) {
    DatabaseApi.delete(entryId, "practice_log_entries");
  }

  bool updateLogEntry(PracticeLogEntry editedEntry) {
    try {
      DatabaseApi.update(editedEntry.id, "practice_log_entries", {
        "id": editedEntry.id,
        "title": editedEntry.title,
        "duration": editedEntry.duration,
        "metronome": editedEntry.metronome,
        "date": editedEntry.date,
        "description": editedEntry.description,
        "tags": editedEntry.tags.join("|"),
      });
      return true;
    } catch (error) {
      return false;
    }
  }
}
