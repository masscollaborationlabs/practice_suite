import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/services/locator_service.dart';
import 'package:practice_suite/services/metronome_service.dart';

part 'metronome_event.dart';

part 'metronome_state.dart';

class MetronomeBloc extends Bloc<MetronomeEvent, MetronomeState> {
  final _metronome = getIt<MetronomeService>();
  int tempo = 100;
  Notes note = Notes.fourth;
  bool tempoAdjustedWhenOn = false;
  String normalSound = "sound-1.wav";
  String accentSound = "sound-1.wav";

  MetronomeBloc() : super(MetronomeInitial()) {
    int setTempoToNoteSelected(Notes selectedNote, int selectedTempo) {
      switch(selectedNote) {
        case Notes.fourth:
          return selectedTempo;
        case Notes.eighth:
          return selectedTempo * 2;
        case Notes.triplet:
          return selectedTempo * 3;
        default:
          return selectedTempo;
      }
    }

    on<InitMetronomeEvent>((event, emit) {
      _metronome.init();
      emit(MetronomeTempoState(tempo: setTempoToNoteSelected(note, tempo)));
      emit(MetronomeSoundState(normalSound: normalSound, accentSound: accentSound));
    });
    on<DisposeMetronomeEvent>((event, emit) {
      _metronome.dispose();
    });
    on<ToggleOnOffMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());
      if (_metronome.isMetronomeOn) {
        await _metronome.off();
      } else {
        await _metronome.on(setTempoToNoteSelected(note, tempo), note);
      }
      emit(ToggleOnOffMetronomeState(isMetronomeOn: _metronome.isMetronomeOn));

      debugPrint("TOGGLED METRONOME ${_metronome.isMetronomeOn ? "ON" : "OFF"}");
    });
    on<IncreaseTempoMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());
      if (_metronome.isMetronomeOn) {
        tempoAdjustedWhenOn = true;
        await _metronome.off();
      //  emit(ToggleOnOffMetronomeState(isMetronomeOn: _metronome.isMetronomeOn));
      }
      if (tempo == (note == Notes.triplet ? 197 : 230)) {
        tempo = 24;
      } else {
        tempo++;
      }
      if (!_metronome.isMetronomeOn && tempoAdjustedWhenOn) {
        await _metronome.on(setTempoToNoteSelected(note, tempo), note);
        emit(ToggleOnOffMetronomeState(isMetronomeOn: _metronome.isMetronomeOn));
        tempoAdjustedWhenOn = false;
      }
      emit(MetronomeTempoState(tempo: tempo));
    });
    on<DecreaseTempoMetronomeEvent>((event, emit) async {
      emit(LoadingMetronomeState());
      if (_metronome.isMetronomeOn) {
        tempoAdjustedWhenOn = true;
        await _metronome.off();
        //  emit(ToggleOnOffMetronomeState(isMetronomeOn: _metronome.isMetronomeOn));
      }
      if (tempo == 24) {
        tempo = note == Notes.triplet ? 197 : 230;
      } else {
        tempo--;
      }
      if (!_metronome.isMetronomeOn && tempoAdjustedWhenOn) {
        await _metronome.on(setTempoToNoteSelected(note, tempo), note);
        emit(ToggleOnOffMetronomeState(isMetronomeOn: _metronome.isMetronomeOn));
        tempoAdjustedWhenOn = false;
      }
      emit(MetronomeTempoState(tempo: tempo));
    });
    on<ChangeTempoMetronomeEvent>((event, emit) {
      emit(LoadingMetronomeState());
      tempo = (note == Notes.triplet && event.givenTempo > 197) ? 197 : event.givenTempo;
      if (_metronome.isMetronomeOn) {
        _metronome.off();
        _metronome.on(setTempoToNoteSelected(note, tempo), note);
      }
      emit(MetronomeTempoState(tempo: tempo));
    });
    on<ChangeMetronomeSoundMetronomeEvent>((event, emit) {
      emit(LoadingMetronomeState());
      normalSound = event.soundName;
      accentSound = event.accentSoundName;
      _metronome.changeMetronomeSound(normalSound, accentSound, setTempoToNoteSelected(note, tempo));
      emit(MetronomeSoundState(normalSound: normalSound, accentSound: accentSound));
    });
    on<ChangeNotesEvent>((event, emit) {
      emit(LoadingMetronomeState());
      note = event.note;
      getIt<MetronomeService>().selectedNote = note;
      if (note != Notes.fourth) {
        emit(MetronomeSoundState(
            normalSound: getIt<MetronomeService>().normalSound,
            accentSound: getIt<MetronomeService>().accentSound));
      }
      if (note == Notes.triplet && tempo > 197) {
        tempo = 197;
        emit(MetronomeTempoState(tempo: tempo));
      }
      if (_metronome.isMetronomeOn) {
        _metronome.off();
        _metronome.on(setTempoToNoteSelected(note, tempo), note);
      }
    });
  }

}
