import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/logic/blocs/practice_log/practice_log_bloc.dart';
import 'package:practice_suite/presentation/widgets/practice_log/practice_log_entry_list.dart';
import 'package:practice_suite/presentation/widgets/practice_log/practice_log_list_filter.dart';
import 'package:practice_suite/presentation/widgets/practice_log/practice_log_tags.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_container.dart';

import '../../services/locator_service.dart';
import '../widgets/practice_log/new_practice_log_entry_sheet.dart';

class PracticeLogScreen extends StatefulWidget {
  const PracticeLogScreen({Key? key}) : super(key: key);

  @override
  State<PracticeLogScreen> createState() => _PracticeLogScreenState();
}

class _PracticeLogScreenState extends State<PracticeLogScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const PTContainer(child: PracticeLogListFilter()),
          BlocBuilder<PracticeLogBloc, PracticeLogState>(
            buildWhen: (previousState, currentState) => currentState is PracticeLogEntries,
            builder: (context, state) {
              return Column(children: [
                if (getIt<PracticeLogBloc>().availableTags.isNotEmpty) ...[
                  const SizedBox(height: 5),
                  const PTContainer(child: PracticeLogTags()),
                ],
              ],);
            },
          ),
          const SizedBox(height: 5),
          const Expanded(
            child: PTContainer(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: PracticeLogEntryList(),
              ),
            ),
          ),
        ],
      ),
      Positioned(
          right: 25,
          bottom: 17,
          child: FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () {
                showModalBottomSheet(
                  backgroundColor: Colors.transparent,
                  context: context,
                  isScrollControlled: true,
                  builder: (_) {
                    return const NewPracticeLogEntrySheet();
                  },
                );
              }))
    ]);
  }
}
