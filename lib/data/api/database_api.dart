import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite/sqlite_api.dart';

class DatabaseApi {
  static Future<Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, "practice_suite.db"),
        onCreate: (db, version) {
      db.execute(
          "CREATE TABLE practice_log_entries(id TEXT PRIMARY KEY, title TEXT, tags TEXT, duration TEXT, metronome TEXT, date TEXT, description TEXT)");
      db.execute(
          "CREATE TABLE recording_log_entries(id TEXT PRIMARY KEY, date TEXT, time TEXT, title TEXT, duration TEXT, fileName TEXT)");
      }, version: 2);
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DatabaseApi.database();
    db.insert(
      table,
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DatabaseApi.database();
    return db.query(table);
  }

  static Future<void> delete(String id, String table) async {
    final db = await DatabaseApi.database();
    db.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  static Future<void> update(
      String id, String table, Map<String, Object> data) async {
    final db = await DatabaseApi.database();
    db.update(table, data, where: 'id = ?', whereArgs: [id]);
  }
}
