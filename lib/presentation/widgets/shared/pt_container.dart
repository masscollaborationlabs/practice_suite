import 'package:flutter/material.dart';

class PTContainer extends StatelessWidget {
  final Widget child;
  final Color? borderColor;
  final EdgeInsets? padding;

  const PTContainer(
      {Key? key, required this.child, this.borderColor, this.padding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.only(left: 8.0, right: 8.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
              style: BorderStyle.solid,
              color: borderColor ?? Theme.of(context).colorScheme.primary),
        ),
        child: child,
      ),
    );
  }
}
