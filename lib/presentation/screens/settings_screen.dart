import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:practice_suite/presentation/widgets/settings/pt_language_widget.dart';
import 'package:practice_suite/presentation/widgets/settings/pt_theme_mode_widget.dart';
import 'package:practice_suite/presentation/widgets/shared/pt_container.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../generated/l10n.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  Future<PackageInfo> _getPackageInfo() {
    return PackageInfo.fromPlatform();
  }

  Widget aboutSection(BuildContext context) {
    return PTContainer(
      child: Column(
        children: [
          const SizedBox(height: 10),
          Text(
            S.of(context).settings_title_about,
            style: Theme.of(context).textTheme.displayLarge,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    const SizedBox(width: 10),
                    Image.asset(
                          "assets/icons/p_t_icon_rounded.png",
                      width: 30,
                      height: 30,
                    ),
                    const SizedBox(width: 15),
                    const Text('Practice Suite'),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: FutureBuilder<PackageInfo>(
                      future: _getPackageInfo(),
                      builder: (BuildContext context,
                          AsyncSnapshot<PackageInfo> snapshot) {
                        final data = snapshot.data;

                        return data != null ? Text(data.version) : const Placeholder();
                      }),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(S.of(context).settings_follow_on),
              GestureDetector(
                onTap: () async {
                  Uri url = Uri.parse(
                      "https://fosstodon.org/@Berker");
                  if (await canLaunchUrl(url)) {
                    await launchUrl(
                      mode: LaunchMode.externalApplication,
                      url,
                    );
                  } else {
                    final snackBar = SnackBar(
                      content: Expanded(
                        child: Text("Error launching $url"),
                      ),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                child: Row(
                  children: const [
                    Text('Mastodon', style: TextStyle(color: Colors.blue)),
                  ],
                ),
              ),
              Text(" & "),
              GestureDetector(
                onTap: () async {
                  Uri url = Uri.parse(
                      "https://twitter.com/berker");
                  if (await canLaunchUrl(url)) {
                    await launchUrl(
                      mode: LaunchMode.externalApplication,
                      url,
                    );
                  } else {
                    final snackBar = SnackBar(
                      content: Expanded(
                        child: Text("Error launching $url"),
                      ),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                child: Row(
                  children: const [
                    Text('Twitter', style: TextStyle(color: Colors.blue)),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(S.of(context).settings_built_with_love),
              GestureDetector(
                onTap: () async {
                  Uri url = Uri.parse(
                      "https://keyoxide.org/0ae2d5b4de5749838fe54d15009be9f2f3baa000");
                  if (await canLaunchUrl(url)) {
                    await launchUrl(
                      mode: LaunchMode.externalApplication,
                      url,
                    );
                  } else {
                    final snackBar = SnackBar(
                      content: Expanded(
                        child: Text("Error launching $url"),
                      ),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                child: Row(
                  children: const [
                    Text(
                      '@',
                      style: TextStyle(color: Colors.blue),
                    ),
                    Text('Berker', style: TextStyle(color: Colors.blue)),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('© Berker Sen - Open Source - '),
              GestureDetector(
                onTap: () async {
                  Uri url = Uri.parse(
                      "https://codeberg.org/Berker/practice_suite/src/branch/master/LICENSE");
                  if (await canLaunchUrl(url)) {
                    await launchUrl(
                      mode: LaunchMode.externalApplication,
                      url,
                    );
                  } else {
                    final snackBar = SnackBar(
                      content: Expanded(
                        child: Text("Error launching $url"),
                      ),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                child: Row(
                  children: const [

                    Text('AGPL-3.0', style: TextStyle(color: Colors.blue)),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(150),
                ),
              ),
              onPressed: () => showLicensePage(context: context),
              child: SizedBox(
                width: 70,
                child: Text(
                  "Licences",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
            ),
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget donateSection(BuildContext context) {
    return PTContainer(
        child: Column(
      children: [
        const SizedBox(height: 10),
        Text(S.of(context).settings_title_donate,
            style: Theme.of(context).textTheme.displayLarge),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            S.of(context).settings_donation_txt,
            textAlign: TextAlign.center,
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: GestureDetector(
            onTap: () async {
              Uri donateUrl = Uri.parse("https://liberapay.com/Berker/");
              if (await canLaunchUrl(donateUrl)) {
                await launchUrl(
                  mode: LaunchMode.externalApplication,
                  donateUrl,
                );
              } else {
                final snackBar = SnackBar(
                  content: Expanded(
                    child: Text("Error launching $donateUrl"),
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text('Liberapay'),
                Icon(Icons.open_in_new_outlined),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: GestureDetector(
            onTap: () async {
              Uri donateUrl = Uri.parse("https://ko-fi.com/b_e_r_k_e_r");
              if (await canLaunchUrl(donateUrl)) {
                await launchUrl(
                  mode: LaunchMode.externalApplication,
                  donateUrl,
                );
              } else {
                final snackBar = SnackBar(
                  content: Expanded(
                    child: Text("Error launching $donateUrl"),
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text('Ko-fi'),
                Icon(Icons.open_in_new_outlined),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: GestureDetector(
            onTap: () async {
              Uri donateUrl = Uri.parse("https://paypal.me/BerkerSen");
              if (await canLaunchUrl(donateUrl)) {
                await launchUrl(
                  mode: LaunchMode.externalApplication,
                  donateUrl,
                );
              } else {
                final snackBar = SnackBar(
                  content: Expanded(
                    child: Text("Error launching $donateUrl"),
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Text('PayPal'),
                Icon(Icons.open_in_new_outlined),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
      ],
    ));
  }

  Widget helpSection(BuildContext context) {
    return PTContainer(
        child: Column(
      children: [
        const SizedBox(height: 10),
        Text(S.of(context).settings_title_help,
            style: Theme.of(context).textTheme.displayLarge),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: GestureDetector(
            onTap: () async {
              Uri email = Uri.parse("mailto:practice_suite@fastmail.com");
              if (await canLaunchUrl(email)) {
                await launchUrl(
                  mode: LaunchMode.externalApplication,
                  email,
                );
              } else {
                final snackBar = SnackBar(
                  content: Expanded(
                    child: Text("Error launching $email"),
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(S.of(context).settings_title_email),
                const Icon(Icons.open_in_new_outlined),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: GestureDetector(
            onTap: () async {
              Uri matrixUrl =
                  Uri.parse("https://matrix.to/#/#practice_suite:matrix.org");
              if (await canLaunchUrl(matrixUrl)) {
                await launchUrl(
                  mode: LaunchMode.externalApplication,
                  matrixUrl,
                );
              } else {
                final snackBar = SnackBar(
                  content: Expanded(
                    child: Text("Error launching $matrixUrl"),
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(S.of(context).settings_title_matrix_room),
                const Icon(Icons.open_in_new_outlined),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: GestureDetector(
            onTap: () async {
              Uri issuesUrl =
              Uri.parse("https://codeberg.org/Berker/practice_suite/issues");
              if (await canLaunchUrl(issuesUrl)) {
                await launchUrl(
                  mode: LaunchMode.externalApplication,
                  issuesUrl,
                );
              } else {
                final snackBar = SnackBar(
                  content: Expanded(
                    child: Text("Error launching $issuesUrl"),
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(S.of(context).settings_title_issues),
                const Icon(Icons.open_in_new_outlined),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: GestureDetector(
            onTap: () async {
              Uri sourceUrl =
              Uri.parse("https://codeberg.org/Berker/practice_suite");
              if (await canLaunchUrl(sourceUrl)) {
                await launchUrl(
                  mode: LaunchMode.externalApplication,
                  sourceUrl,
                );
              } else {
                final snackBar = SnackBar(
                  content: Expanded(
                    child: Text("Error launching $sourceUrl"),
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(S.of(context).settings_title_source),
                const Icon(Icons.open_in_new_outlined),
              ],
            ),
          ),
        ),
        const SizedBox(height: 10),
      ],
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text(
            S.of(context).settings_title_txt,
            style: Theme.of(context).textTheme.headlineSmall,
          )),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const PTLanguageWidget(),
            const SizedBox(height: 10),
            const PTThemeModeWidget(),
            donateSection(context),
            const SizedBox(height: 10),
            helpSection(context),
            const SizedBox(height: 10),
            aboutSection(context),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
