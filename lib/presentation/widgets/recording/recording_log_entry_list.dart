import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:practice_suite/constants/enums.dart';
import 'package:practice_suite/data/api/sound_api.dart';
import 'package:practice_suite/data/models/recording_log_entry.dart';
import 'package:practice_suite/generated/l10n.dart';
import 'package:practice_suite/logic/blocs/recording_log/recording_log_bloc.dart';
import 'package:practice_suite/presentation/widgets/shared/delete_confirmation_dialog.dart';
import 'package:share_plus/share_plus.dart';

class RecordingLogEntrylist extends StatefulWidget {
  const RecordingLogEntrylist({Key? key}) : super(key: key);

  @override
  _RecordingLogEntrylistState createState() => _RecordingLogEntrylistState();
}

class _RecordingLogEntrylistState extends State<RecordingLogEntrylist> {
  late SoundApi audioPlayer;
  bool _isPlaying = false;
  String _fileNamePlaying = "";

  @override
  void initState() {
    audioPlayer = SoundApi();
    audioPlayer.initPlayerOnly();
    BlocProvider.of<RecordingLogBloc>(context).add(FetchRecordingLog());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    audioPlayer.disposePlayerOnly();
  }

  TWhenFinished? _whenFinishedPlaying() {
    _isPlaying = false;
    setState(() {});
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<RecordingLogBloc, RecordingLogState>(
        builder: (context, state) {
      List<RecordingLogEntry> logEntries = [];

      if (state is LoadingRecordingLogState) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
      if (state is RecordingLogEntries) {
        logEntries = state.recordingLog;

        return logEntries.isEmpty
            ? Center(
                child: Text(S.of(context).youHaventRecordedAnythingYet),
              )
            : SingleChildScrollView(
                child: Column(
                  children: logEntries
                      .map((RecordingLogEntry entry) => ListTile(
                            visualDensity:
                                VisualDensity.adaptivePlatformDensity,
                            title: Text(entry.title),
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${entry.date} - ${entry.time}"),
                                Text(entry.duration),
                              ],
                            ),
                            dense: true,
                            trailing: SizedBox(
                              width: 150,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  IconButton(
                                      visualDensity: VisualDensity.compact,
                                      onPressed: () async {
                                        if (_isPlaying) {
                                          _isPlaying = false;
                                          await audioPlayer.stopPlaying();
                                          setState(() {});
                                        } else {
                                          _isPlaying = true;
                                          _fileNamePlaying = entry.fileName;
                                          setState(() {});
                                          await audioPlayer.play(entry.fileName,
                                              _whenFinishedPlaying);
                                        }
                                      },
                                      icon: Icon(
                                          _isPlaying &&
                                                  _fileNamePlaying ==
                                                      entry.fileName
                                              ? Icons.stop
                                              : Icons.arrow_right,
                                          size: 30)),
                                  IconButton(
                                      visualDensity: VisualDensity.compact,
                                      onPressed: () {
                                        Share.shareXFiles(
                                          [XFile('${audioPlayer.pathToSaveAudio}${entry.fileName}.wav')]
                                        ,
                                            subject: S
                                                .of(context)
                                                .share_recording_title,
                                            text: S
                                                .of(context)
                                                .share_recording_text);
                                      },
                                      icon: const Icon(Icons.share)),
                                  IconButton(
                                      visualDensity: VisualDensity.compact,
                                      onPressed: () {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return DeleteConfirmationDialog(
                                                id: entry.id,
                                                title: entry.title,
                                                entryType:
                                                    EntryType.recordingLog,
                                                filePath: entry.fileName,
                                              );
                                            });
                                      },
                                      icon: const Icon(
                                        Icons.delete,
                                        color: Colors.red,
                                      ))
                                ],
                              ),
                            ),
                          ))
                      .toList(),
                ),
              );
      }
      return Container();
    }, listener: (context, state) {
      void showSnackBar(String message) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(message),
          duration: const Duration(seconds: 2),
        ));
      }

      if (state is AddedEntry) {
        showSnackBar(S.of(context).entryAddedSuccessfully);
      }
      if (state is RemovedEntry) {
        showSnackBar(S.of(context).entryRemovedSuccessfully);
      }
      if (state is UpdatedEntry) {
        showSnackBar(S.of(context).entryUpdatedSuccessfully);
      }
      if (state is RecordingLogFailureState) {
        showSnackBar(state.errorMessage);
      }
    });
  }
}
