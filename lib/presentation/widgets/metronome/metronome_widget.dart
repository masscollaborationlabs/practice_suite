import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:practice_suite/logic/blocs/metronome/metronome_bloc.dart';
import 'package:practice_suite/presentation/widgets/metronome/sound_picker.dart';
import 'package:practice_suite/services/locator_service.dart';
import 'package:practice_suite/services/metronome_service.dart';

import '../../../generated/l10n.dart';

class MetronomeWidget extends StatefulWidget {
  const MetronomeWidget({Key? key}) : super(key: key);

  @override
  _MetronomeWidgetState createState() => _MetronomeWidgetState();
}

class _MetronomeWidgetState extends State<MetronomeWidget> {
  Timer? timer;
  late int tempo;
  bool _isMetronomeOn = false;
  bool _tempoAdjustedWhenOn = false;
  late MetronomeBloc _bloc;
  Notes notes = Notes.fourth;
  bool _isFourthNote = true;
  bool _isEighthNote = false;
  bool _isTripletNote = false;

  @override
  void initState() {
    super.initState();
    _bloc = getIt<MetronomeBloc>();
    _bloc.add(InitMetronomeEvent());
    tempo = _bloc.tempo;
    notes = _bloc.note;
    _selectNotes();
  }

  @override
  void dispose() {
    _bloc.add(DisposeMetronomeEvent());
    _isMetronomeOn = false;
    super.dispose();
  }

  void _selectNotes() {
    switch (notes) {
      case Notes.fourth:
        _isFourthNote = true;
        _isEighthNote = false;
        _isTripletNote = false;
        break;
      case Notes.eighth:
        _isFourthNote = false;
        _isEighthNote = true;
        _isTripletNote = false;
        break;
      case Notes.triplet:
        _isFourthNote = false;
        _isEighthNote = false;
        _isTripletNote = true;
        break;
      default:
        _isFourthNote = true;
        _isEighthNote = false;
        _isTripletNote = false;
        break;
    }
    getIt<MetronomeBloc>().add(ChangeNotesEvent(note: notes));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MetronomeBloc, MetronomeState>(
      bloc: _bloc,
      builder: (context, state) {
        if (state is ToggleOnOffMetronomeState) {
          _isMetronomeOn = state.isMetronomeOn;
        }
        if (state is MetronomeTempoState) {
          tempo = state.tempo;
        }

        return Stack(children: [
          Positioned(
            top: -10,
            right: -10,
            child: IconButton(
                iconSize: 20,
                splashRadius: 15,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (_) {
                        return Dialog(
                            backgroundColor: Theme.of(context)
                                .dialogBackgroundColor
                                .withOpacity(0.8),
                            insetPadding: const EdgeInsets.all(60),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child:
                                  Text(S.of(context).metronome_triplet_warning),
                            ));
                      });
                },
                icon: const Icon(Icons.info_outlined)),
          ),
          Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ChoiceChip(
                        selected: _isFourthNote,
                        onSelected: (value) {
                          setState(() {
                            notes = Notes.fourth;
                            _selectNotes();
                          });
                        },
                        label: Image.asset(
                          "assets/icons/fourth_icon.png",
                          height: 30,
                          width: 50,
                        ),
                      ),
                      const SizedBox(width: 10),
                      ChoiceChip(
                        selected: _isEighthNote,
                        onSelected: (value) {
                          setState(() {
                            notes = Notes.eighth;
                            _selectNotes();
                          });
                        },
                        label: Image.asset(
                          "assets/icons/eighth_icon.png",
                          height: 30,
                          width: 50,
                        ),
                      ),
                      const SizedBox(width: 10),
                      ChoiceChip(
                        selected: _isTripletNote,
                        onSelected: (value) {
                          setState(() {
                            notes = Notes.triplet;
                            _selectNotes();
                          });
                        },
                        label: Image.asset(
                          "assets/icons/triplet_icon.png",
                          height: 30,
                          width: 50,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  SoundPicker(isAccentSoundEnabled: !_isFourthNote),
                  const SizedBox(height: 15),
                  GestureDetector(
                      onTapDown: (TapDownDetails details) {
                        if (_isMetronomeOn) {
                          _bloc.add(ToggleOnOffMetronomeEvent());
                          _tempoAdjustedWhenOn = true;
                        }
                        timer = Timer.periodic(
                            const Duration(milliseconds: 100), (t) {
                          _bloc.add(IncreaseTempoMetronomeEvent());
                        });
                      },
                      onTapUp: (TapUpDetails details) {
                        timer?.cancel();
                      },
                      onTapCancel: () {
                        timer?.cancel();
                        if (_tempoAdjustedWhenOn) {
                          _bloc.add(ToggleOnOffMetronomeEvent());
                          _tempoAdjustedWhenOn = false;
                        }
                      },
                      child: FloatingActionButton.small(
                          heroTag: 'plusButton',
                          onPressed: () {
                            setState(() {
                              _bloc.add(IncreaseTempoMetronomeEvent());
                            });
                          },
                          child: const Icon(Icons.add))),
                  const SizedBox(height: 15),
                  Text(tempo.toString(), style: const TextStyle(fontSize: 20)),
                  const SizedBox(height: 15),
                  GestureDetector(
                      onTapDown: (TapDownDetails details) {
                        if (_isMetronomeOn) {
                          _bloc.add(ToggleOnOffMetronomeEvent());
                          _tempoAdjustedWhenOn = true;
                        }
                        timer = Timer.periodic(
                            const Duration(milliseconds: 100), (t) {
                          _bloc.add(DecreaseTempoMetronomeEvent());
                        });
                      },
                      onTapUp: (TapUpDetails details) {
                        timer?.cancel();
                      },
                      onTapCancel: () {
                        timer?.cancel();
                        if (_tempoAdjustedWhenOn) {
                          _bloc.add(ToggleOnOffMetronomeEvent());
                          _tempoAdjustedWhenOn = false;
                        }
                      },
                      child: FloatingActionButton.small(
                          heroTag: 'minusButton',
                          onPressed: () {
                            setState(() {
                              _bloc.add(DecreaseTempoMetronomeEvent());
                            });
                          },
                          child: const Icon(Icons.remove))),
                ],
              ),
            ),
          ),
          Positioned(
              bottom: 16,
              right: 16,
              child: FloatingActionButton(
                onPressed: () {
                  _bloc.add(ToggleOnOffMetronomeEvent());
                },
                child: buildOnOffIcon(),
              ))
        ]);
      },
    );
  }
}

extension _Helpers on _MetronomeWidgetState {
  Icon buildOnOffIcon() {
    return Icon(
      Icons.power_settings_new,
      color: _isMetronomeOn
          ? Colors.red
          : Theme.of(context).floatingActionButtonTheme.backgroundColor,
    );
  }
}
