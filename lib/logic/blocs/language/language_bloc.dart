import 'dart:ui';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/api/shared_prefs_api.dart';

part 'language_event.dart';
part 'language_state.dart';

class LanguageBloc extends Bloc<LanguageEvent, LanguageState> {
  LanguageBloc() : super(ChangeLanguageState(appLanguage: SharedPrefsApi.getLanguage())) {
    Locale language =
    SharedPrefsApi.getLanguage();
    on<ChangeLanguageEvent>((event, emit) {
      emit(LoadingLanguageState());
      language = event.appLanguage;
      SharedPrefsApi.setLanguage('${event.appLanguage.languageCode}_${event.appLanguage.countryCode}');
      emit(ChangeLanguageState(appLanguage: event.appLanguage));
    });
  }
}
