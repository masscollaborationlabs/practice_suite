part of 'metronome_bloc.dart';

abstract class MetronomeEvent extends Equatable {
  const MetronomeEvent();

  @override
  List<Object> get props => [];
}

class InitMetronomeEvent extends MetronomeEvent {}
class DisposeMetronomeEvent extends MetronomeEvent {}
class ToggleOnOffMetronomeEvent extends MetronomeEvent {}
class IncreaseTempoMetronomeEvent extends MetronomeEvent {}
class DecreaseTempoMetronomeEvent extends MetronomeEvent {}
class LoadingMetronomeEvent extends MetronomeEvent {}
class ChangeNotesEvent extends MetronomeEvent {
  final Notes note;

  const ChangeNotesEvent({required this.note});

  @override
  List<Object> get props => [note];

  @override
  String toString() {
    return 'ChangeNotesEvent{Note: $note}';
  }
}
class ChangeTempoMetronomeEvent extends MetronomeEvent {
  final int givenTempo;
  const ChangeTempoMetronomeEvent({required this.givenTempo});

  @override
  List<Object> get props => [givenTempo];

  @override
  String toString() {
    return 'ChangeTempoMetronomeEvent{givenTempo: $givenTempo}';
  }
}

class ChangeMetronomeSoundMetronomeEvent extends MetronomeEvent {
  final String soundName;
  final String accentSoundName;
  const ChangeMetronomeSoundMetronomeEvent({required this.soundName, required this.accentSoundName});
  @override
  List<Object> get props => [soundName];

  @override
  String toString() {
    return 'ChangeMetronomeSoundMetronomeEvent{soundName: $soundName, accent sound name: $accentSoundName}';
  }
}
