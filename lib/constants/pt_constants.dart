/// KEYS
const String themeModeKey = 'themeMode';
const String languageKey = 'language';
const String firstLaunchKey = 'firstLaunch';

/// VALUES
const String darkModeValue = 'darkMode';
const String lightModeValue = 'lightMode';
const String systemModeValue = 'systemMode';
const String englishValue = 'en_US';
const String germanValue = 'de_DE';
const String turkishValue = 'tr_TR';
const String systemLanguageValue = 'systemLanguage';