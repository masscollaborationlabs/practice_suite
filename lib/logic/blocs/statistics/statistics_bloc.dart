import 'dart:async';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:practice_suite/data/models/practice_duration.dart';
import 'package:practice_suite/data/models/statistic_charts.dart';
import 'package:practice_suite/presentation/widgets/statistics/weekly_statistics_chart.dart';
import 'package:practice_suite/presentation/widgets/statistics/yearly_statistics_percent_chart.dart';
import 'package:practice_suite/utils/date_time_utils.dart';

import '../../../generated/l10n.dart';
import '../../../presentation/widgets/statistics/statistic_grid_tile.dart';
import '../../../services/locator_service.dart';
import '../practice_log/practice_log_bloc.dart';

part 'statistics_event.dart';

part 'statistics_state.dart';

class StatisticsBloc extends Bloc<StatisticsEvent, StatisticsState> {
  List<PanelItem> _practiceLogItems = [];
  List<Widget> statistics = [];
  StatisticCharts charts = StatisticCharts(
      thisWeek: const Placeholder(),
      lastWeek: const Placeholder(),
      thisYear: const Placeholder(),
      thisMonth: const Placeholder());

  Future<void> _fetchAndBuildStatistics() async {
    statistics = [];
    _practiceLogItems = getIt<PracticeLogBloc>().practiceLogItems;
    if (_practiceLogItems.isNotEmpty) {
      statistics.add(_getAvarageMetronomeSpeed());
      statistics.add(_getTimePracticedThisMonth());
      statistics.add(_getTotalTimePracticed());

      charts.thisWeek = _getHoursPracticedThisOrLastWeek(lastWeek: false);
      charts.lastWeek = _getHoursPracticedThisOrLastWeek(lastWeek: true);
      charts.thisYear = _getTimePracticedThisYear();
    }
  }

  Widget _getAvarageMetronomeSpeed() {
    int totalLogEntryCountWithMetronome = 0;
    int totalMetronomeSpeed = 0;

    for (var logItem in _practiceLogItems) {
      int? tempo = int.tryParse(logItem.logEntry.metronome);
      if (tempo != null) {
        totalLogEntryCountWithMetronome++;
        totalMetronomeSpeed += tempo;
      }
    }
    return StatisticGridTile(
        title: getIt<S>().statistics_title_average_tempo,
        value:
            "${(totalMetronomeSpeed / totalLogEntryCountWithMetronome).round()} bpm");
  }

  Widget _getHoursPracticedThisOrLastWeek({required bool lastWeek}) {
    List<double> totalHoursPracticed = [0, 0, 0, 0, 0, 0, 0];
    int totalMinutes = 0;

    DateTime toTime = DateTime.now();
    DateTime fromTime = DateTime(toTime.year, toTime.month, toTime.day);

    DateTime firstDayOfWeek = lastWeek
        ? DateTimeUtils().findFirstDateOfPreviousWeek(fromTime)
        : DateTimeUtils().findFirstDateOfTheWeek(fromTime);
    DateTime lastDayOfWeek = lastWeek
        ? DateTimeUtils().findLastDateOfPreviousWeek(toTime)
        : DateTimeUtils().findLastDateOfTheWeek(toTime);
    for (var logEntry in _practiceLogItems) {
      DateTime entryDate = DateTime.parse(logEntry.logEntry.date);
      if ((entryDate.isAfter(firstDayOfWeek) ||
              entryDate.isAtSameMomentAs(firstDayOfWeek)) &&
          (entryDate.isBefore(lastDayOfWeek) ||
              entryDate.isAtSameMomentAs(lastDayOfWeek))) {
        totalMinutes += int.parse(logEntry.logEntry.duration);
        PracticeDuration duration =
            PracticeDuration.fromMinutes(int.parse(logEntry.logEntry.duration));
        totalHoursPracticed[entryDate.weekday - 1] +=
            double.parse("${duration.hour}.${duration.minute}");
      }
    }
    PracticeDuration totalHoursAsDuration =
        PracticeDuration.fromMinutesReturnInHoursAndMinutes(totalMinutes);

    return WeeklyStatisticsChart(
        title: lastWeek
            ? getIt<S>().statistics_title_last_week
            : getIt<S>().statistics_title_this_week,
        stats: totalHoursPracticed,
        totalHoursMinutes: totalHoursAsDuration);
  }

  Widget _getTimePracticedThisMonth() {
    int totalTimePracticed = 0;
    DateTime toTime = DateTime.now();
    DateTime fromTime = DateTime(toTime.year, toTime.month, toTime.day);
    DateTime firstDayOfTheMonth =
        DateTimeUtils().findFirstDateOfTheMonth(fromTime);
    DateTime lastDayOfTheMonth = DateTimeUtils().findLastDateOfTheMonth(toTime);

    for (var logEntry in _practiceLogItems) {
      DateTime entryDate = DateTime.parse(logEntry.logEntry.date);
      if (entryDate.isAfter(firstDayOfTheMonth) &&
          entryDate.isBefore(lastDayOfTheMonth)) {
        totalTimePracticed += int.parse(logEntry.logEntry.duration);
      }
    }
    PracticeDuration duration = totalTimePracticed < 1440
        ? PracticeDuration.fromMinutesReturnInHoursAndMinutes(
            totalTimePracticed)
        : PracticeDuration.fromMinutes(totalTimePracticed);

    return StatisticGridTile(
        title: getIt<S>().statistics_time_practiced_this_month,
        value: totalTimePracticed < 1440
            ? getIt<S>().statistics_hours_and_minutes(
                duration.hour.toString(), duration.minute.toString())
            : getIt<S>().statistics_days_hours_minutes(duration.day.toString(),
                duration.hour.toString(), duration.minute.toString()));
  }

  Widget _getTimePracticedThisYear() {
    int totalTimePracticed = 0;
    List<double> hoursInmonths = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    DateTime toTime = DateTime.now();
    DateTime fromTime = DateTime(toTime.year, toTime.month, toTime.day);
    DateTime firstDayOfTheYear =
        DateTimeUtils().findFirstDateOfTheYear(fromTime);
    DateTime lastDayOfTheYear = DateTimeUtils().findLastDateOfTheYear(toTime);

    for (var logEntry in _practiceLogItems) {
      DateTime entryDate = DateTime.parse(logEntry.logEntry.date);
      if ((entryDate.isAfter(firstDayOfTheYear) ||
              entryDate.isAtSameMomentAs(firstDayOfTheYear)) &&
          (entryDate.isBefore(lastDayOfTheYear) ||
              entryDate.isAtSameMomentAs(lastDayOfTheYear))) {
        totalTimePracticed += int.parse(logEntry.logEntry.duration);
        hoursInmonths[entryDate.month - 1] +=
            int.parse(logEntry.logEntry.duration);
      }
    }

    /// Convert hours to percents.
    List<double> hourPercentsInMonths = [];

    for (var hours in hoursInmonths) {
      if (hours == 0) {
        hourPercentsInMonths.add(0);
      } else {
        hourPercentsInMonths.add(hours / totalTimePracticed * 100);
      }
    }

    PracticeDuration duration = totalTimePracticed < 1440
        ? PracticeDuration.fromMinutesReturnInHoursAndMinutes(
            totalTimePracticed)
        : PracticeDuration.fromMinutes(totalTimePracticed);

    return YearlyStatisticsPercentChart(
      practicedHoursInMonths: hourPercentsInMonths,
      year: firstDayOfTheYear.year.toString(),
      totalPracticeTime: duration,
      totalPracticedHours: totalTimePracticed,
    );
  }

  Widget _getTotalTimePracticed() {
    int totalTimePracticed = 0;

    for (var logEntry in _practiceLogItems) {
      totalTimePracticed += int.parse(logEntry.logEntry.duration);
    }
    PracticeDuration duration = totalTimePracticed < 1440
        ? PracticeDuration.fromMinutesReturnInHoursAndMinutes(
            totalTimePracticed)
        : PracticeDuration.fromMinutes(totalTimePracticed);

    return StatisticGridTile(
        title: getIt<S>().statistics_total_time_practiced,
        value: totalTimePracticed < 1440
            ? getIt<S>().statistics_hours_and_minutes(
                duration.hour.toString(), duration.minute.toString())
            : getIt<S>().statistics_days_hours_minutes(duration.day.toString(),
                duration.hour.toString(), duration.minute.toString()));
  }

  StatisticsBloc() : super(StatisticsInitial()) {
    on<FetchAndBuildStatisticsEvent>((event, emit) async {
      emit(LoadingStatisticsState());
      await _fetchAndBuildStatistics();
      emit(FetchAndBuildSuccessState(statistics: statistics, charts: charts));
    });
  }
}
