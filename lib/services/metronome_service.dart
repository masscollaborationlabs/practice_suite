import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;

import 'package:flutter_sound/flutter_sound.dart';

// ValueNotifier<int> tempo = ValueNotifier(100);

List<String> metronomeSounds = [
  "sound-1.wav",
  "sound-2.wav",
  "sound-3.wav",
  "sound-4.wav",
  "sound-5.wav",
  "sound-6.wav",
  "sound-7.wav"
];

enum Notes {
  fourth(1),
  eighth(2),
  triplet(3);

  const Notes(this.value);

  final int value;
}

class MetronomeService {
  bool _isMetronomeOn = false;

  get isMetronomeOn => _isMetronomeOn;

  late Timer _timer;
  late int _startTimestamp;
  late int _nextBeat;
  late Uint8List normalSoundByteData;
  late Uint8List accentSoundByteData;
  late FlutterSoundPlayer? _audioPlayer;
  String normalSound = "sound-1.wav";
  String accentSound = "sound-1.wav";
  Notes selectedNote = Notes.fourth;

  final _sampleRate = 44100;

  // Will be computed from time signature
  int _accentSoundInterval = 1;

  Future init() async {
    _audioPlayer = FlutterSoundPlayer();
    _audioPlayer?.openPlayer();

    // Load the sound byte data
    normalSoundByteData = await convertSoundToByteData(normalSound);
    accentSoundByteData = await convertSoundToByteData(accentSound);
  }

  Future dispose() async {
    if (isMetronomeOn) {
      await off();
    }
  }

  Future<Uint8List> convertSoundToByteData(String soundPath) async {
    return (await rootBundle.load('assets/sounds/$soundPath'))
        .buffer
        .asUint8List();
  }

  Future off() async {
    if (isMetronomeOn) {
      _isMetronomeOn = false;

      _audioPlayer?.stopPlayer();
      _timer.cancel();
    }
  }

  Future on(int tempo, Notes note) async {
    selectedNote = note;
    _accentSoundInterval = selectedNote.value;
    if (!isMetronomeOn) {
      _isMetronomeOn = true;
      _startTimestamp = DateTime.now().millisecondsSinceEpoch;
      _nextBeat = 0;

      final intervalLengthSeconds = 60 / tempo;
      final intervalLengthSamples =
          (intervalLengthSeconds * _sampleRate).round();
      // Sound is played at 16 bits per sample, Uint8List uses 8 bit elements,
      // so each sample requires two elements in the Uint8List
      final intervalLengthUint8List = intervalLengthSamples * 2;

      // Generate the byte data in between sounds
      var gapByteData = List.generate(
          intervalLengthUint8List - normalSoundByteData.length, (i) => 0);

      // Transform Uint8List to food for the foodsink
      var food =
          FoodData(Uint8List.fromList(normalSoundByteData + gapByteData));
      var food2 =
          FoodData(Uint8List.fromList(accentSoundByteData + gapByteData));

      // Start the audio player
      await _audioPlayer?.startPlayerFromStream(
          codec: Codec.pcm16, numChannels: 1, sampleRate: _sampleRate);

      // Feed the first beat
      _audioPlayer!.foodSink?.add(food);

      _timer = Timer.periodic(const Duration(milliseconds: 10), (timer) {
        // Calculate the timestamp (in ms since UNIX epoch) until next beat
        var nextBeatTimestamp =
            _startTimestamp + (60000 / tempo).round() * _nextBeat;
        // If current timestamp is later than next beat timestamp,
        // queue the next beat
        if (DateTime.now().millisecondsSinceEpoch >= nextBeatTimestamp) {
          if (_nextBeat % _accentSoundInterval == _accentSoundInterval - 1) {
            // Queue the 1st beat sound
            _audioPlayer!.foodSink?.add(food);
          } else {
            // Queue the other beat sound
            _audioPlayer!.foodSink?.add(food2);
          }
          // Increment the beat counter
          _nextBeat++;
        }
      });
    }
  }

  Future<void> changeMetronomeSound(
      String soundName, String accentSoundName, int tempo) async {
    normalSound = soundName;
    accentSound = selectedNote == Notes.fourth ? soundName : accentSoundName;
    normalSoundByteData = await convertSoundToByteData(normalSound);
    accentSoundByteData = await convertSoundToByteData(accentSound);
    if (isMetronomeOn) {
      off();
      on(tempo, selectedNote);
    }
  }
}
