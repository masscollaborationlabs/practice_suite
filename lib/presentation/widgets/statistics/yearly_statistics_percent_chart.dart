import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:practice_suite/data/models/practice_duration.dart';

import '../../../generated/l10n.dart';

class YearlyStatisticsPercentChart extends StatefulWidget {
  final String year;
  final int totalPracticedHours;
  final PracticeDuration totalPracticeTime;
  final List<double> practicedHoursInMonths;

  const YearlyStatisticsPercentChart(
      {super.key,
      required this.practicedHoursInMonths,
      required this.totalPracticedHours,
      required this.year,
      required this.totalPracticeTime});

  @override
  _YearlyStatisticsPercentState createState() =>
      _YearlyStatisticsPercentState();
}

class _YearlyStatisticsPercentState
    extends State<YearlyStatisticsPercentChart> {
  int touchedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: Card(
        elevation: 0,
        color: Colors.transparent,
        child: Row(
          children: <Widget>[
            const SizedBox(
              height: 18,
            ),
            Expanded(
              child: AspectRatio(
                aspectRatio: 1,
                child: Column(
                  children: [
                    Text(
                      "${S.of(context).statistics_title_year}: ${widget.year}",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).brightness == Brightness.light
                            ? Colors.black
                            : Colors.white,
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      widget.totalPracticedHours < 1440
                          ? "${S.of(context).statistics_txt_total}: ${S.of(context).statistics_hours_and_minutes(widget.totalPracticeTime.hour, widget.totalPracticeTime.minute)}"
                          : "${S.of(context).statistics_txt_total}: ${S.of(context).statistics_days_hours_minutes(widget.totalPracticeTime.day, widget.totalPracticeTime.hour, widget.totalPracticeTime.minute)}",
                      style: TextStyle(
                        color: Theme.of(context).brightness == Brightness.light
                            ? Colors.black54
                            : Colors.white54,
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Expanded(
                      child: PieChart(
                        PieChartData(
                          pieTouchData: PieTouchData(
                            touchCallback:
                                (FlTouchEvent event, pieTouchResponse) {
                              setState(() {
                                if (!event.isInterestedForInteractions ||
                                    pieTouchResponse == null ||
                                    pieTouchResponse.touchedSection == null) {
                                  touchedIndex = -1;
                                  return;
                                }
                                touchedIndex = pieTouchResponse
                                    .touchedSection!.touchedSectionIndex;
                              });
                            },
                          ),
                          borderData: FlBorderData(
                            show: false,
                          ),
                          sectionsSpace: 0,
                          centerSpaceRadius: 40,
                          sections:
                              showingSections(widget.practicedHoursInMonths),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            FittedBox(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.blue,
                    text: S.of(context).statistics_month_january,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.amber,
                    text: S.of(context).statistics_month_february,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.brown,
                    text: S.of(context).statistics_month_march,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.cyanAccent,
                    text: S.of(context).statistics_month_april,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.deepOrange,
                    text: S.of(context).statistics_month_may,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.green,
                    text: S.of(context).statistics_month_june,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.deepPurpleAccent,
                    text: S.of(context).statistics_month_july,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.red,
                    text: S.of(context).statistics_month_august,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.yellowAccent,
                    text: S.of(context).statistics_month_september,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.lightGreenAccent,
                    text: S.of(context).statistics_month_october,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.grey,
                    text: S.of(context).statistics_month_november,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Colors.pinkAccent,
                    text: S.of(context).statistics_month_december,
                    isSquare: true,
                    textColor: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 28,
            ),
          ],
        ),
      ),
    );
  }

  List<PieChartSectionData> showingSections(
      List<double> practicedHoursInMonths) {
    List<PieChartSectionData> chartList = [];

    practicedHoursInMonths.asMap().forEach((index, value) {
      if (value != 0) {
        final isTouched = index == touchedIndex;
        final fontSize = isTouched ? 25.0 : 16.0;
        final radius = isTouched ? 60.0 : 50.0;
        chartList.add(PieChartSectionData(
          color: getChartSectionColor(index),
          value: value,
          title: value == 100 ? '${value.round()}%' : '$value%',
          radius: radius,
          titleStyle: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).brightness == Brightness.light
                ? Colors.black
                : Colors.white,
          ),
        ));
      }
    });
    return chartList;
  }
}

Color getChartSectionColor(int index) {
  switch (index) {
    case 0:
      return Colors.blue;
    case 1:
      return Colors.amber;
    case 2:
      return Colors.brown;
    case 3:
      return Colors.cyanAccent;
    case 4:
      return Colors.deepOrange;
    case 5:
      return Colors.green;
    case 6:
      return Colors.deepPurpleAccent;
    case 7:
      return Colors.red;
    case 8:
      return Colors.yellowAccent;
    case 9:
      return Colors.lightGreenAccent;
    case 10:
      return Colors.grey;
    case 11:
      return Colors.pinkAccent;
    default:
      return Colors.blue;
  }
}

class Indicator extends StatelessWidget {
  const Indicator({
    super.key,
    required this.color,
    required this.text,
    required this.isSquare,
    this.size = 16,
    required this.textColor,
  });

  final Color color;
  final String text;
  final bool isSquare;
  final double size;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: isSquare ? BoxShape.rectangle : BoxShape.circle,
            color: color,
          ),
        ),
        const SizedBox(
          width: 4,
        ),
        Text(
          text,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: textColor,
          ),
        )
      ],
    );
  }
}
