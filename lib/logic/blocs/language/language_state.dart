part of 'language_bloc.dart';

abstract class LanguageState extends Equatable {
  const LanguageState();
}

class ChangeLanguageState extends LanguageState {
  final Locale appLanguage;


  const ChangeLanguageState({required this.appLanguage});

  @override
  List<Object?> get props => [];

}

class LoadingLanguageState extends LanguageState {
  @override
  List<Object?> get props => [];
}
