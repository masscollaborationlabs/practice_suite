import 'package:flutter/material.dart';
import 'package:practice_suite/constants/enums.dart';
import 'package:practice_suite/logic/blocs/language/language_bloc.dart';

import '../../../data/api/shared_prefs_api.dart';
import '../../../generated/l10n.dart';
import '../../../services/locator_service.dart';

class PTLanguageWidget extends StatefulWidget {
  const PTLanguageWidget({Key? key}) : super(key: key);

  @override
  State<PTLanguageWidget> createState() => _PTLanguageWidgetState();
}

class _PTLanguageWidgetState extends State<PTLanguageWidget> {
  late Locale selectedLanguage;

  selectLanguage(Locale lang) {
    selectedLanguage = lang;
    getIt<LanguageBloc>()
        .add(ChangeLanguageEvent(appLanguage: selectedLanguage));
  }

  @override
  void initState() {
    selectedLanguage = SharedPrefsApi.getLanguage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Text(S.of(context).settings_title_language),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: DropdownButton(borderRadius: BorderRadius.circular(20),
              value: selectedLanguage,
              items: [
                DropdownMenuItem(alignment: Alignment.center,
                    value: PTLanguage.english.locale,
                    child: Text(getIt<S>().settings_language_english)),
                DropdownMenuItem(alignment: Alignment.center,
                    value: PTLanguage.deutsch.locale,
                    child: Text(getIt<S>().settings_language_german)),
                DropdownMenuItem(alignment: Alignment.center,
                    value: PTLanguage.turkce.locale,
                    child: Text(S.of(context).settings_language_turkish)),
              ],
              onChanged: (selected) {
                selectLanguage(selected as Locale);
              }),
        ),
      ],
    );
  }
}
