part of 'recording_log_bloc.dart';

abstract class RecordingLogEvent extends Equatable {
  const RecordingLogEvent();

  @override
  List<Object> get props => [];
}

class FetchRecordingLog extends RecordingLogEvent {}

class LoadingRecordingLog extends RecordingLogEvent {}

class AddRecordingLogEntry extends RecordingLogEvent {
  final RecordingLogEntry newRecordingLogEntry;
  const AddRecordingLogEntry(this.newRecordingLogEntry);

  @override
  List<Object> get props => [newRecordingLogEntry];

  @override
  String toString() {
    return "AddRecordingLogEntry { Entry: $newRecordingLogEntry }";
  }
}

class RemoveRecordingLogEntry extends RecordingLogEvent {
  final String recordingLogEntryID;
  final String filePath;
  const RemoveRecordingLogEntry(this.recordingLogEntryID, this.filePath);

  @override
  List<Object> get props => [recordingLogEntryID];

  @override
  String toString() {
    return "RemoveRecordingLogEntry { Entry: $recordingLogEntryID }";
  }
}

class UpdateRecordingLogEntry extends RecordingLogEvent {
  final RecordingLogEntry updatedRecordingLogEntry;
  const UpdateRecordingLogEntry(this.updatedRecordingLogEntry);

  @override
  List<Object> get props => [updatedRecordingLogEntry];

  @override
  String toString() {
    return "UpdateRecordingLogEntry { Entry: $updatedRecordingLogEntry }";
  }
}

class RecordingLogFailure extends RecordingLogEvent {
  final String errorMessage;

  const RecordingLogFailure({required this.errorMessage});
}

class RecordingAudio extends RecordingLogEvent {}
class RecorderIdle extends RecordingLogEvent {}
class PlayingAudio extends RecordingLogEvent {}
class PlayerIdle extends RecordingLogEvent {}
class RecordingAvailable extends RecordingLogEvent {}


