part of 'practice_log_bloc.dart';

abstract class PracticeLogEvent extends Equatable {
  const PracticeLogEvent();

  @override
  List<Object> get props => [];
}

class FetchPracticeLog extends PracticeLogEvent {}

class LoadingPracticeLog extends PracticeLogEvent{}

class AddPracticeLogEntry extends PracticeLogEvent {
  final PracticeLogEntry newPracticeLogEntry;
  const AddPracticeLogEntry(this.newPracticeLogEntry);

  @override
  List<Object> get props => [newPracticeLogEntry];

  @override
  String toString() {
    return "AddPracticeLogEntry { Entry: $newPracticeLogEntry }";
  }
}

class RemovePracticeLogEntry extends PracticeLogEvent {
  final String practiceLogEntryID;
  const RemovePracticeLogEntry(this.practiceLogEntryID);

  @override
  List<Object> get props => [practiceLogEntryID];

  @override
  String toString() {
    return "RemovePracticeLogEntry { Entry: $practiceLogEntryID }";
  }
}

class UpdatePracticeLogEntry extends PracticeLogEvent {
  final PracticeLogEntry updatedPracticeLogEntry;
  const UpdatePracticeLogEntry(this.updatedPracticeLogEntry);

  @override
  List<Object> get props => [updatedPracticeLogEntry];

  @override
  String toString() {
    return "UpdatePracticeLogEntry { Entry: $updatedPracticeLogEntry }";
  }
}

class FilterEntriesInDateRange extends PracticeLogEvent {
  final DateTime startDate;
  final DateTime endDate;
  final bool showAllEntries;
  const FilterEntriesInDateRange({required this.startDate, required this.endDate, required this.showAllEntries});

  @override
  List<Object> get props => [startDate, endDate];

  @override
  String toString() {
    return "Filter entries in range: { Start Date: $startDate , End Date: $endDate}, Show All: $showAllEntries";
  }
}

class FilterEntriesWithTags extends PracticeLogEvent {
  final Map<String, bool> selectedTags;

  const FilterEntriesWithTags({required this.selectedTags});

  @override
  List<Object> get props => [selectedTags];
}





